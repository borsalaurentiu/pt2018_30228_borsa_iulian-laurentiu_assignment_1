package HW2;

import java.util.*; 

public class Coada extends Thread{
	private Vector<Client> clienti = new Vector<Client>(); 
	
	public Coada( String name ){
		setName( name );
	}
	
	public void run(){
		try{
			while( true ){
				stergeClient();
				sleep( (int) (Math.random()*4000) );
			}
		 }
		 catch( InterruptedException e ){
			 System.out.println("Intrerupere");
			 System.out.println( e.toString());
		 	}
		 }
	
	public synchronized void adaugaClient(Client client) throws InterruptedException{
		clienti.addElement(client);
		notifyAll();
	
	}
	
	public synchronized void stergeClient() throws InterruptedException{
		while( clienti.size() == 0 )
		wait();
		Client client = clienti.elementAt(0);
		clienti.removeElementAt(0);
		ClientiGUI.text += ("Clientul " + Integer.toString( client.getID())+" a fost deservit la "+getName() + "\n");
		notifyAll();
	}
	
	public synchronized int lungimeCoada() throws InterruptedException{
		notifyAll();
		int size = clienti.size();
		return size;
	}

	public Vector<Client> getClienti() {
		return clienti;
	}

	public void setClienti(Vector<Client> clienti) {
		this.clienti = clienti;
	}
	
}
