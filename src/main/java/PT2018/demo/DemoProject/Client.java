package HW2;

import java.util.Random;

public class Client {
	private int clientID;
	private int timpSosire;
	private int timpProcesare;
	private int timpPlecare;
	
	public Client(int clientID, int timpSosire, int timpProcesare){
		this.timpSosire = timpSosire;
		this.timpProcesare = timpProcesare;
		this.clientID = clientID;
	}
	
	public Client() {
	
	}
	public int getID() {
		return clientID;
	}
	public void setID(int clientID) {
		this.clientID = clientID;
	}
	public int getTimpSosire() {
		return timpSosire;
	}
	public int getTimpProcesare() {
		return timpProcesare;
	}

	public int getTimpPlecare() {
		return timpPlecare;
	}

	public void setTimpSosire(int timpSosire) {
		this.timpSosire = timpSosire;
	}

	public void setTimpProcesare(int timpProcesare) {
		this.timpProcesare = timpProcesare;
	}

	public void setTimpPlecare(int timpAsteptare) {
		timpPlecare = timpSosire + timpProcesare + timpAsteptare;
	}
	
	public String toString() {
		return ("Clientul " + Integer.toString(clientID) + ": timp de sosire "+Integer.toString(timpSosire)+" si timp de procesare "+Integer.toString(timpProcesare) + ". \n");
	}
}