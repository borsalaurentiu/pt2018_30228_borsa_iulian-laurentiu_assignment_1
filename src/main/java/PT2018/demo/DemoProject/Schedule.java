package HW2;
import java.util.*;

public class Schedule extends Thread{
	private Coada coada[];
	private int numarCozi;
	static int ID;
	private int numarClienti;


	public Schedule(int numarCozi, Coada coada[], String nume, int numarClienti){
		setName (getName());
		this.numarCozi = numarCozi;
		this.coada = new Coada[numarCozi];
		this.numarClienti = numarClienti;
		for( int i=0; i< numarCozi; i++){
			this.coada[i] = coada[i] ; 	
		}
	}

	private int min_index (){
		int index = 0;
		try{
			int min = coada[0].lungimeCoada();
			for( int i=1; i < numarCozi; i++){
				int lung = coada[ i ].lungimeCoada();
				if ( lung < min ){
					min = lung;
					index = i;
				}
			}
		}
		catch( InterruptedException e ){
			System.out.println( e.toString());
		}
		return index;
	}

	@Override
	public void run(){
		try{
			int i=0;
			while( i<numarClienti ){
				i++;
				Client client = new Client( ++ID, ClientiGUI.timpSosire, ClientiGUI.timpProcesare);
				int m = min_index();
				ClientiGUI.text += ("Clientul " + Integer.toString(client.getID())+" a fost adaugat la coada "+ Integer.toString(m) + "\n");
				coada[m].adaugaClient(client);
				sleep( (int) (Math.random()*1000) );
			}
		}
		catch( InterruptedException e ){
			System.out.println( e.toString());
		}
	}
}
